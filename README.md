# getting started

1. fork repo
1. git clone your fork
1. install npm packages `npm install`
1. testing:
   - unit ([jest](https://jestjs.io/)): `npm run test:unit`
   - visual ([nightwatch](http://nightwatchjs.org/)): `npm run test:e2e`
   - lint ([eslint](https://eslint.org/)): `npm run lint`
1. create merge request from your fork

# tasks

> **NB:** make each task as a separate commit!

 * `lint`: fix all linting errors (do not make new errors in future)
 * `visual`: use google
    - open [https://www.google.ee/](https://www.google.ee/)
    - search word `tallinn`
    - check that result contains word `tallinn` and make screenshot
    - open first result and take screenshot
 * `visual`: use tpt homepage
    - open [https://www.tptlive.ee/](https://www.tptlive.ee/), take screenshot
    - go `Tunniplaan`, take screenshot
    - open your class timetable, take screenshot
 * `unit`: use snapshot testing to test `src/user.js` (use ids 1, 56 and 1345)
 * `unit`: use string as input(id) for `src/user.js` - test that exception is given
 * `unit`: write implementation (`capitalize.js`) for this test (you can use npm packages in `capitalize.js`)
```js
// capitalize.spec.js
const capitalize = require('../../src/capitalize');

describe('sum', () => {
  it('triin => Triin', () => {
    expect(capitalize('triin')).toBe("Triin");
  });
  it('error - bad input', () => {
    expect(() => {
      capitalize({ "word": 'cat' });
    }).toThrow(/bad input/);
  });
});
```
 * `unit`: test `src/randomPlus.js` while mocking `src/random.js` (check from mock that random params was 1 and 99)
