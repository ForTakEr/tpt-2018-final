var config = require('../../nightwatch.conf.js');

module.exports = {
    'example use google': function(browser) {
        browser
            .url('http://google.com')
            .waitForElementVisible('body div#main', 1000)
            .setValue('input[type=text]', 'Tallinn')
            .pause(2000)
            .click('input[name=btnK]')
            .pause(2000)
            .assert.containsText('.bkWMgd:first-child', 'Tallinn')
            .saveScreenshot(config.imgpath(browser) + 'tallinnsearch.png')
            .click('.LC20lb:first-child')
            .pause(2000)
            .saveScreenshot(config.imgpath(browser) + 'result.png')
            .end();
    }
};