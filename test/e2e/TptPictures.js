var config = require('../../nightwatch.conf.js');

module.exports = {
    'tptlive': function(browser) {
        browser
            .resizeWindow(1900, 1000)
            .url('https://www.tptlive.ee/')
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'tptlive.png')
            .click("#menu-item-1313")
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'tunniplaan.png')
            .useXpath()
            .click("//a[text()='TA-17E']")
            .useCss()
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'myTimetable.png')
            .end();
    }
};
